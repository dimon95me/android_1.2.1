package examplesoureit.android_1_21;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.text)
    EditText text;
    int counter = 0;

    ArrayDeque<String> deque = new ArrayDeque<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.action_clear)
    void clear() {
        deque.addFirst(text.getText().toString());
        text.setText("");
        text.setHint("Поле очищено");
        counter = 0;
        if(deque.size()>5){
            while(deque.size()>5){
                deque.removeLast();
            }
        }
    }

    @OnClick(R.id.action_restore)
    void restore() {
        counter++;
        if(counter>5){
            Toast.makeText(this, "Возвратилось больше 5", Toast.LENGTH_SHORT);
            return;
        }

        try {
                text.setText(deque.pollFirst());
        } catch (EmptyStackException e) {
            text.setText("Values are end");
        }
    }
}